import { Component, OnInit } from '@angular/core';
import { TestService } from './test.service';
import { Data, Item } from './entities'
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
    public list: Data[];
    public current: Data;
    private subscribtions: Subscription[] = [];
    constructor(private db: TestService) { }
    async ngOnInit() {
        this.list = this.db.get_list();
        let subscription = this.db.save(this.list[1]).subscribe((response: any) => {
            console.log(`Post data to backend result:\n${response._body}`)
        })
        this.subscribtions.push(subscription);
        let id = await this.select(this.list[3]);
        return id;
    }
    ngOnDestroy() {
        this.subscribtions.forEach(s => {
            if (s) { s.unsubscribe(); }
        })
        this.subscribtions = null;
    }
    public select(data: Data) {
        return new Promise((resolve, reject) => {
            this.current = data;
            if (!data.items) {
                let subscription = this.db.get_by(data.id).subscribe((response: Data) => {
                    data.count = response.count;
                    data.items = response.items;
                    data.items.forEach(i => i.lock = false);
                    let percent_sum = data.items.reduce((p, c) => { return p += c.percent }, 0);
                    if (percent_sum < 100) { this.change(100, data.items[0]); }
                    if (percent_sum > 100) {
                        data.items.forEach(i => i.percent = i.count = 0);
                        this.change(100, data.items[0]);
                    }
                    resolve(data.id)
                }, (err) => { console.log(err); reject() })
                this.subscribtions.push(subscription);
            }
        })
    }
    public change(e: number, item: Item, isCount = false) {
        let valid_input = isCount ? this.isInt(e) : (this.isInt(e) || this.isFloat(e))
        if (!valid_input) { this.fix(item) }
        else {
            let target = isCount ? item.count : item.percent;
            let isAdd = e > target;
            let diff = Math.abs(e - target);
            let { count, percent } = isCount ? this.request(null, diff) : this.request(diff);
            if (isAdd && count > 0) this.add(item, count, percent);
            if (!isAdd && count > 0) this.delete(item, count, percent);
        }
    }
    private isInt(n) { return Number(n) === n && n % 1 === 0; }
    private isFloat(n) { return Number(n) === n && n % 1 !== 0; }
    public round4(number) { return Math.round(number * 100000000) / 100000000; }
    private add(to: Item, count: number, percent: number) {
        let free_count = this.current.items.reduce((p, c) => { return p - c.count }, this.current.count);
        if (free_count >= count) {
            to.count += count;
            to.percent = this.round4(to.percent + percent);
        }
        else {
            this.add(to, free_count, this.request(null, free_count).percent);
            let withdraw_count = count - free_count;
            let withdraw_challengers = this.current.items
                .filter(i => { return !i.lock && i != to && i.count > 0 })
                .sort((a, b) => { return b.count - a.count });
            if (withdraw_challengers.length > 0) {
                let first = withdraw_challengers[0];
                let { count, percent } = this.request(null, Math.min(first.count, withdraw_count));
                this.move(first, to, count, percent);
                withdraw_count -= count;
                if (withdraw_count > 0) {
                    let { count, percent } = this.request(null, withdraw_count);
                    this.add(to, count, percent);
                }
            }
        }
    }
    private delete(from: Item, d_count: number, d_percent: number) {
        if (this.current.items.length == 1) {
            let { count, percent } = this.request(null, d_count);
            from.count -= count;
            from.percent = this.round4(from.percent - percent);
        }
        else {
            let add_challengers = this.current.items
                .filter(i => { return !i.lock && i != from && i.percent < 100 })
                .sort((a, b) => { return a.count - b.count });
            if (add_challengers.length > 0) {
                let first = add_challengers[0];
                if ((first.percent + d_percent) <= 100) {
                    let { count, percent } = this.request(null, d_count);
                    this.move(from, first, count, percent);
                }
            }
        }
    }
    private move(from: Item, to: Item, count: number, percent: number) {
        from.count -= count;
        from.percent = this.round4(from.percent - percent);
        this.add(to, count, percent);
    }
    private request(request_percent, request_count = null) {
        let count = request_count ? request_count : Math.round(this.current.count * request_percent / 100);
        let percent = this.round4(count * 100 / this.current.count);
        return { count: count, percent: percent };
    }
    public change_debounce = this.debounce(this.number_input, 300)
    public number_input(e: number, item: Item, isCount = false) {
        this.change(e, item, isCount);
        this.fix(item);
    }
    debounce(f, ms) {
        let timer = null;
        return (...args) => {
            const onComplete = () => {
                f.apply(this, args);
                timer = null;
            }
            if (timer) { clearTimeout(timer); }
            timer = setTimeout(onComplete, ms);
        };
    }
    public fix(item: Item) {
        item.count++;
        item.percent++;
        setTimeout(() => {
            item.count--;
            item.percent--;
        }, 0)
    }
    public fix_debounce = this.debounce(this.fix, 50)
    public slider_change(number, item){
        this.change(number, item);
    }
}
