import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { TestService } from './test.service';
import { TestComponent } from './test.component';
import { MatCardModule } from '@angular/material/card';
import { MatSliderModule } from '@angular/material/slider';
import { MatRadioModule } from '@angular/material/radio';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { MockHttpClient } from './mock_http.service';
import { MockBackend } from "@angular/http/testing";
import { BaseRequestOptions } from "@angular/http";

@NgModule({
    imports: [CommonModule, HttpClientModule, BrowserAnimationsModule, FormsModule,
        MatCardModule, MatSliderModule, MatRadioModule, MatIconModule, MatInputModule
    ],
    declarations: [TestComponent],
    exports: [TestComponent]
})
export class TestModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: TestModule,
            providers: [
                TestService, 
                MockBackend, BaseRequestOptions,
                { provide: MockHttpClient, useClass: MockHttpClient }
            ]
        }
    }
}
