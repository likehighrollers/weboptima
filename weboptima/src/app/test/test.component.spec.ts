import { fakeAsync, async, ComponentFixture, TestBed, inject, tick } from '@angular/core/testing';

import { TestComponent } from './test.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';
import { MatSliderModule } from '@angular/material/slider';
import { MatRadioModule } from '@angular/material/radio';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { MockHttpClient } from './mock_http.service';
import { MockBackend } from "@angular/http/testing";
import { BaseRequestOptions } from "@angular/http";
import { TestService } from './test.service';
import { Item } from './entities';

describe('TestComponent', () => {
    let component: TestComponent;
    let fixture: ComponentFixture<TestComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TestComponent],
            imports: [FormsModule, HttpClientModule, BrowserAnimationsModule,
                MatCardModule, MatSliderModule, MatRadioModule, MatIconModule, MatInputModule
            ],
            providers: [
                TestService, 
                MockBackend, BaseRequestOptions,
                { provide: MockHttpClient, useClass: MockHttpClient }
            ]
        }).compileComponents();
    }));

    beforeEach((done) => {
        fixture = TestBed.createComponent(TestComponent);
        component = fixture.componentInstance;
        component.ngOnInit().then((id)=>{
            fixture.detectChanges();
            done()
        })
    });

    it('item allocation when 1 item less 0.01 percent', (done) => {
        component.select(component.list[3]).then((id)=>{
            let c = component.current; 
            expect(c.items[0].percent).toEqual(100);
            component.change(0.01, c.items[1]);
            expect_item_allocation(c.items[0], 99.99, 99990000);
            expect_item_allocation(c.items[1], 0.01, 10000);
            component.change(0.02, c.items[1]);
            expect_item_allocation(c.items[0], 99.98, 99980000);
            expect_item_allocation(c.items[1], 0.02, 20000);
            component.change(19999, c.items[1], true);
            expect_item_allocation(c.items[0], 99.98, 99980000);
            expect_item_allocation(c.items[1], 0.02, 19999);
            expect_item_allocation(c.items[2], 0.00, 1);
            component.change(19998, c.items[1], true);
            expect_item_allocation(c.items[0], 99.98, 99980000);
            expect_item_allocation(c.items[1], 0.02, 19998);
            expect_item_allocation(c.items[2], 0.00, 1);
            expect_item_allocation(c.items[3], 0.00, 1);
            fixture.detectChanges();
            done()
        })
    });
});

function expect_item_allocation(item: Item, percent, count){
    expect(Math.round(item.percent*100)/100).toEqual(percent);
    expect(item.count).toEqual(count);
}