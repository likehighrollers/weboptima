import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Data, Item } from './entities'
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/delayWhen';
import 'rxjs/add/operator/mergeMap';
import { MockHttpClient } from './mock_http.service';

@Injectable()
export class TestService {
    constructor(private http: HttpClient, private mock_http: MockHttpClient) {
        mock_http.start()
    }
    public get_list(): Data[] {
        return [
            { name: 'Одно свойсво', id: 1 },
            { name: 'Два свойсва', id: 2 },
            { name: 'Пять свойств', id: 5 },
            { name: 'Семь свойств', id: 7 },
            { name: 'Десять свойств', id: 10 }
        ]
    }
    public get_by(id: number) {
        return this.http.get(`assets/${id}.json`).retryWhen(err => { return this.complex_retry(err) })
    }
    public save(data: Data) {
        return this.mock_http.post(`assets/${data.id}.json`, data).retryWhen(err => { return this.complex_retry(err) })
    }
    private complex_retry(errors: Observable<any>, count: number = 11) {
        return errors
            .mergeMap((error) => {
                return 400 <= error.status && error.status < 500
                    ? Observable.throw(error)
                    : Observable.of(error)
            })
            .scan((retryCount) => {
                retryCount += 1;
                if (retryCount <= count) { return retryCount; }
                else { throw ({ error: 'Not available' }) }
            }, 0)
            .delayWhen(time => Observable.interval(2000 * 2 ** (time - 1)))
    }
}
