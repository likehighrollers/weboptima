import { Injectable } from "@angular/core";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { ResponseOptions, Response, Http, BaseRequestOptions } from "@angular/http";

@Injectable()
export class MockHttpClient extends Http {
    constructor(private backend: MockBackend, defaultOptions: BaseRequestOptions) {
        super(backend, defaultOptions);
    }
    start(): void {
        this.backend.connections.subscribe((c: MockConnection) => {
            if (c.request.method === 1){
                let idRegex = /assets\/([0-9]+).json/i;
                let id = c.request.url.match(idRegex)[1]
                c.mockRespond(new Response(new ResponseOptions({
                    body: `item with id: ${id} saved`
                })));
            }
        });
    }
}