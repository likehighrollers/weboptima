export interface Data {
    id: number;
    name: string;
    count?: number;
    items?: Item[];
}
export interface Item {
    name: string;
    count: number;
    percent: number;
    lock: boolean;
}